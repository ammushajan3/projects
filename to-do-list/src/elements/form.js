import styled from "styled-components";
export const Cross = styled.span`
  color: lightpink;
  font-weight: 900;
  visibility: hidden;
  &:hover {
    color: brown;
  }
`;
export const Input = styled.input`
  padding: 16px 16px 16px 60px;
  border: none;
  background: white;
  box-shadow: inset 0 -2px 1px rgba(0, 0, 0, 0.03);
  width: 100%;
  font-size: 24px;
  font-family: inherit;
  font-weight: inherit;
  margin-top: -34px;
  &:focus{
    outline:none;
  }
`;
export const CheckInput = styled.div`
  padding: 16px 16px 16px 60px;
  display: inline-block;
  background: white;
  box-shadow: inset 0 -2px 1px rgba(0, 0, 0, 0.03);
  width: 100%;
  font-size: 24px;
  font-family: inherit;
  font-weight: inherit;
  display: flex;
  justify-content: space-around;
  text-decoration: ${props => (props.checked ? "line-through" : "none")};
  opacity: ${props => (props.checked ? 0.5 : 3)};
  &:hover ${Cross} {
    visibility: visible;
  }
`;

export const Options = styled.div`
  padding: 16px 16px 16px 60px;
  display: inline-block;
  background: white;
  border: none;
  box-shadow: 0 1px 1px rgba(0, 0, 0, 0.2), 0 8px 0 -3px #f6f6f6,
    0 9px 1px -3px rgba(0, 0, 0, 0.2), 0 16px 0 -6px #f6f6f6,
    0 17px 2px -6px rgba(0, 0, 0, 0.2);
  width: 100%;
  font-size: 24px;
  font-family: inherit;
  font-weight: inherit;
  display: flex;
  justify-content: space-around;
`;

export const Uncheck = styled.div`
  padding: 16px 16px 16px 60px;
  display: inline-block;
  background: whitesmoke;
  border: 1px solid black;
  width: 100%;
  font-size: 24px;
  font-family: inherit;
  font-weight: inherit;
`;
