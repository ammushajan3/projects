import React from "react";
import { CheckInput } from "./form";
import { Label } from "./button";
import { Cross } from "./form";
import "./../App.css"

export default function search(props) {
  return (
    <div>
      {props.array.map((data, index) => (
        <CheckInput>
          <div>
            <input className="form-radio"
              type="checkbox"
              checked={data.checked}
              onClick={() => props.click(data.id)}
            ></input>
            <Label checked={data.checked}>{data.status} </Label>
          </div>
          <Cross>
            <i
              onClick={props.delete.bind(index)}
              class="fa fa-times"
              aria-hidden="true"
            ></i>{" "}
          </Cross>
        </CheckInput>
      ))}
    </div>
  );
}
