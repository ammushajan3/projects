import styled,{css} from "styled-components";
export default styled.h1`
  font-size:100px;
  font-weight: 100;
  font-family: Helvetica, Arial, sans-serif;
  ${props =>
    props.color &&
    css`
      color: ${props => props.theme[props.color]};
    `};
`;
