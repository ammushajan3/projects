import styled from "styled-components";
export const Button = styled.button`
 margin: 3px;
  padding: 3px 7px;
    border:none;
    border-radius: 3px;

  &:hover {
    border: 1px solid rgba(175, 47, 47, 0.2);
  }

  /* background-color: ${props => (props.primary ? "red" : "green")}; */
 

`;
export const Item = styled.span`
  margin: 3px;
  font-size: 16px;
`;

export const Clear = styled.button`
  font-size: 16px;
  margin: 3px;
  padding: 3px 7px;
  border: none;
  border-radius: 3px;
  background: whitesmoke;
`;

export const Label = styled.label`
  text-decoration: ${props => (props.checked ? "line-through" : "none")};
  opacity: ${props => (props.checked ? 0.5 : 3)};
`;

export const Dropdown = styled.span`
  opacity: 0.5;
`;
