import styled from "styled-components";
export default styled.input`
  padding: 16px 16px 16px 60px;
  border: none;
  background: whitesmoke;
  box-shadow: inset 0 -2px 1px rgba(0, 0, 0, 0.03);
  width: 100%;
  font-size: 100px;
  

`;
