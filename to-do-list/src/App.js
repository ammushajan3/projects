import React, { Component } from "react";
import { ThemeProvider } from "styled-components";
import "./App.css";
// import Button from "./elements/button";
import theme from "./elements/theme";
import Todo from "./elements/todo";
import { Input } from "./elements/form";
import Search from "./elements/search";
import { Options } from "./elements/form";
import { Button, Item, Clear,Dropdown } from "./elements/button";

export default class App extends Component {
  statusID = 0;
  state = {
    searchArray: [],
    searchStatus: "",
    id: "",
    checked: false,
    toggle: false
  };

  changeStatus = event => {
    this.setState({ searchStatus: event.target.value });
    // console.log(this.state.searchStatus);
  };

  deleteStatus = index => {
    const copysearchArray = Object.assign([], this.state.searchArray);
    copysearchArray.splice(index, 1);
    this.setState({ searchArray: copysearchArray });
  };
  setStatus = element => {
    this.setState({ searchStatus: element.target.value });
  };
  add = event => {
    if (event.key === "Enter" && this.state.searchStatus !== "") {
      this.statusID = this.statusID + 1;
      const copysearchArray = [...this.state.searchArray];
      let obj = {};
      obj = {
        status: this.state.searchStatus,
        checked: false,
        id: this.statusID
      };
      copysearchArray.push(obj);

      this.setState({
        searchArray: copysearchArray,
        searchStatus: "",
        toggle: true
      });
      // console.log(this.state.searchArray);
    }
  };
  Uncheck = () => {
    const copysearchArray = this.state.searchArray;

    copysearchArray.forEach(element => {
      element.checked = true;
    });
    this.setState({ searchArray: copysearchArray });
  };
  handleChange = () => {
    // this.Uncheck();
    const copysearchArray = this.state.searchArray;

    copysearchArray.forEach(element => {
      element.checked = !element.checked;
    });
    this.setState({ searchArray: copysearchArray });
  };

  handleClick = id => {
    const newsearchArray = this.state.searchArray;
    newsearchArray.forEach(element => {
      if (id === element.id) {
        element.checked = !element.checked;
      }
    });
    this.setState({ searchArray: newsearchArray });
  };
  clear = () => {
    const copysearchArray = this.state.searchArray.filter(
      element => (element.checked === false)
    );
    this.setState({ searchArray: copysearchArray });
  };
  
  render() {
    let options = null;
    let dropdown = null;
    
    if (this.state.toggle && this.state.searchArray.length !== 0) {
      options = (
        <Options>
          <Item>{this.state.searchArray.length}item(s) left</Item>
          <Button>All</Button>
          <Button>Active</Button>
          <Button>Completed</Button>
          <Clear onClick={this.clear}>Clear Completed</Clear>
        </Options>
      );
      dropdown = (
        <Dropdown>
        <i
          onClick={this.handleChange}
          class="fa fa-angle-down dropdown"
          aria-hidden="true"
        ></i>
        </Dropdown>
      );
     
    }
    return (
      <ThemeProvider theme={theme}>
        <div className="App">
          <Todo color="todocolor">todos</Todo>
          {dropdown}
          <Input
            placeholder="What needs to be done?"
            type="text"
            value={this.state.searchStatus}
            onChange={this.setStatus}
            // onChange={this.changeStatus}
            onKeyPress={this.add}
          ></Input>

          {/* <div>
            {this.state.searchArray.map((data, index) => {
              return ( */}
          <Search
            // key={this.state.searchArray.id}
            //id={this.state.searchArray.id}
            //status={this.state.searchArray.status}
            // checked={data.checked}
            array={this.state.searchArray}
            delete={this.deleteStatus}
            click={this.handleClick}
          ></Search>
          {options}
        
          {/* );
            })}
          </div> */}
        </div>
      </ThemeProvider>
    );
  }
}
