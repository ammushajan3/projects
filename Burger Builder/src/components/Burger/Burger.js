import React from "react";
import { Burger } from "./BurgerStyles";
import BurgerIngredient from "./BurgerIngredient";

const burger = (props) => {
  let transformedIngredient = Object.keys(props.ingredients).map(
    (ingredientKey) => {
      return [...Array(props.ingredients[ingredientKey])].map((_, i) => {
        return (
          <BurgerIngredient
            type={ingredientKey}
            key={ingredientKey + i}
          ></BurgerIngredient>
        );
      });
    }
  ).reduce((arr,ele)=>
  {
    return arr.concat(ele);
  },[])
  if(transformedIngredient.length <= 0)
  {
    transformedIngredient=<p>Please add some ingredients !!!</p>
  }
  return (
    <Burger>
      <BurgerIngredient type="bread-top"></BurgerIngredient>
      {transformedIngredient}
      <BurgerIngredient type="bread-bottom"></BurgerIngredient>
 
    </Burger>
  );
};

export default burger;


