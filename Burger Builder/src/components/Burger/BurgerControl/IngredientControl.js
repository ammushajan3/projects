import React from "react";
import "./IngredientControl.css"
const ingredientcontrol = (props) => {
  return (
    <div className="IngredientControl">
      <div className="Label">{props.label}</div>
      <button onClick={props.addIngredient}className="More">Add</button>
      <button onClick={props.removeIngredient} disabled={props.disabledValue}className="Less">Remove</button>
    </div>
  );
};

export default ingredientcontrol;
