import React from "react";
import IngredientControl from "./IngredientControl";

import "./BurgerControl.css";
const ingredients = [
  { label: "Salad", type: "salad" },
  { label: "Meat", type: "meat" },
  { label: "Bacon", type: "bacon" },
  { label: "Cheese", type: "cheese" },
];
const burgercontrol = (props) => {
  
  return (
    <div className="BurgerControl">
      <div>
        Total price :<strong> &#8377;{props.price}</strong>
      </div>
      {ingredients.map((control) => (
        <IngredientControl
          key={control.label}
          label={control.label}
          addIngredient={() => props.addIngredient(control.type)}
          removeIngredient={() => props.removeIngredient(control.type)}
          disabledValue={props.disabledValue[control.type]}
        ></IngredientControl>
      ))}
      {/* <button
        onClick={props.order}
        className="OrderButton"
        disabled={!props.purchasable}
      >
        Check Out !!!
      </button> */}
    </div>
  );
};

export default burgercontrol;
