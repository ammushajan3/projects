import React from 'react'
import {ToolBarHeader} from "./ToolBarStyles"
import "./ToolBar.css"
export default function ToolBar() {
    return (
        <ToolBarHeader>
            <div>Menu</div>
            <div>
            <img className="Logo" src={require("../assets/images/burger.png")} alt="Burger" />
            </div>
            <nav>
                ...
            </nav>
        </ToolBarHeader>
    )
}
