import styled from "styled-components";

export const ToolBarHeader = styled.div`
  height: 40px;
  width: 100%;
  display:flex;
  justify-content:space-between;
  background-color: #DAD735;
  padding : 0 20px;
`;


