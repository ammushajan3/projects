import React, { useState } from 'react';
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import "./OrderSummary.css";
export default function OrderSummary(props) {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);

  const orderSummary = Object.keys(props.ingredients).map((igKey) => {
    return (
      <li key={igKey}>
        <span className="ingredients">{igKey}</span>:{props.ingredients[igKey]}
      </li>
    );
  });
  return (
    <div
      // style={{
      //   transform: props.showOrder ? "translateY(0)" : "translateY(-100vh)",
      //   opacity: props.showOrder ? "1" : "0",
      // }}
    >
      <Modal.Dialog>
        <Modal.Header>
          <Modal.Title>Your Order Summary</Modal.Title>
        </Modal.Header>

        <Modal.Body>
          <p>You have ordered a burger with following incgredients</p>
          <ul>{orderSummary}</ul>
          <div>
            Total price :<strong> &#8377;{props.price}</strong>
          </div>
          {/* <div>Continue to Checkout</div> */}
        </Modal.Body>

        
      </Modal.Dialog>
    </div>
  );
}
