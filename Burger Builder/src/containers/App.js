import React, { Component } from "react";
import Layout from "../components/Layout/Layout";
import Burger from "../components/Burger/Burger";
import BurgerControl from "../components/Burger/BurgerControl/BurgerControl";
import ToolBar from "../components/ToolBar/ToolBar"
import TransitionModal from "../components/TransitionModal/TransitionModal";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
const INGREDIENT_PRICES = {
  salad: 25,
  meat: 50,
  cheese: 25,
  bacon: 50,
};
export default class App extends Component {
  state = {
    ingredients: {
      salad: 0,
      meat: 0,
      cheese: 0,
      bacon: 0,
    },
    totalPrice: 50,
    purchasable: false,
    showOrder: false,
  };

  updateCheckOut = (ingredients) => {
    const sumOfIngredient = Object.keys(ingredients)
      .map((igkey) => {
        return ingredients[igkey];
      })
      .reduce((sum, ele) => {
        return sum + ele;
      }, 0);
    this.setState({ purchasable: sumOfIngredient > 0 });
  };
  addIngredientHandler = (type) => {
    const oldCount = this.state.ingredients[type];
    const newCount = oldCount + 1;
    const updatedIngredient = {
      ...this.state.ingredients,
    };
    updatedIngredient[type] = newCount;
    const oldPrice = this.state.totalPrice;
    const newPrice = INGREDIENT_PRICES[type] + oldPrice;
    this.setState({ totalPrice: newPrice, ingredients: updatedIngredient });
    this.updateCheckOut(updatedIngredient);
  };
  removeIngredientHandler = (type) => {
    const oldCount = this.state.ingredients[type];
    if (oldCount <= 0) {
      return;
    }
    const newCount = oldCount - 1;
    const updatedIngredient = {
      ...this.state.ingredients,
    };
    updatedIngredient[type] = newCount;
    const oldPrice = this.state.totalPrice;
    const newPrice = oldPrice - INGREDIENT_PRICES[type];
    this.setState({ totalPrice: newPrice, ingredients: updatedIngredient });
    this.updateCheckOut(updatedIngredient);
  };
  showOrderHandler = () => {
    this.setState({ showOrder: true });
    console.log("order clicked");
  };
  render() {
    const disabledInfo = {
      ...this.state.ingredients,
    };
    for (let key in disabledInfo) {
      disabledInfo[key] = disabledInfo[key] <= 0 ? true : false;
    }
    return (
      <div>
       <ToolBar></ToolBar>
        <div className="App">
          <Layout></Layout>

          <Burger ingredients={this.state.ingredients}></Burger>

          <BurgerControl
            addIngredient={this.addIngredientHandler}
            removeIngredient={this.removeIngredientHandler}
            disabledValue={disabledInfo}
            price={this.state.totalPrice}
            purchasable={this.state.purchasable}
            order={this.showOrderHandler}
          ></BurgerControl>

          <TransitionModal
            ingredients={this.state.ingredients}
            price={this.state.totalPrice}
            purchasable={this.state.purchasable}
          ></TransitionModal>
        </div>
      </div>
    );
  }
}
